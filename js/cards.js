//seção para apresentar os cards
var $cards = document.querySelector(".cards");

//função para mostrar os cards armazenados
function loadCards(){
  var cards = getCards();

  var newCard =  '<div class="newCard-card col s12 m6 l4">'
              +'<a href="#!" class="newCard-action btn-floating waves-effect waves-light red" onclick="addCard()">'
                +'<i class="material-icons">add</i>'
              +'</a>'
              +'<div class="card blue-grey darken-1">'
                +'<div class="card-content white-text">'
                  +'<div class="card-title">'
                    +'<input placeholder="Titulo" type="text" class="newCard-title validate">'
                  +'</div>'
                  +'<div class="card-content">'
                    +'<textarea placeholder="Conteúdo do Card" class="newCard-content materialize-textarea"></textarea>'
                  +'</div>'
                +'</div>'
              +'</div>'

            +'</div>';

  $cards.innerHTML = newCard;

  for(var index = cards.length-1; index >= 0; index--){
    if(cards[index] != null){
      $cards.innerHTML += '<div class="col s12 m6 l4">'
          +'<div class="card blue-grey darken-1">'
          +'<a href="#!" class="deleteCard btn-floating waves-effect waves-light red" onclick="deleteCards('+index+')">'
            +'<i class="material-icons">delete</i>'
          +'</a>'
            +'<div id='+index+' class="card-content white-text" onclick="editCard(this)" onblur="saveCard(this)">'
              +'<span class="card-title">'+ cards[index].title +'</span>'
              +'<p class="content">'+ cards[index].content +'</p>'
            +'</div>'
          +'</div>'
      +'</div>';
    }
  }
}

//carrega os cards na tela
loadCards();

function addCard(){
  //Valores informados
  var titulo = document.querySelector(".newCard-title").value;
  var conteudo = document.querySelector(".newCard-content").value;

  //valida se um título ou um conteudo foram informados
  if(titulo != "" || conteudo != ""){
    //cria card
    createCard(titulo, conteudo);

    //mostra os cards
    loadCards();
  }
}

function editCard(element){
  element.setAttribute('contenteditable', 'true');
}

function saveCard(element){
  var titulo = element.querySelector(".card-title").textContent;
  var conteudo = element.querySelector(".content").textContent;

  updateCard(element.getAttribute("id"), titulo, conteudo);
  element.setAttribute("contenteditable", "false")
}
