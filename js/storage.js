function getCards(){
  var db = localStorage.getItem("db");

  if(db != undefined && db != null){
    db = JSON.parse(db);
  }else{
    db = {
      "cards" : []
    };
    saveCards(db.cards);
  }

  if(db.cards == undefined || db.cards == null){
    db.cards = [];
    saveCards(db.cards);
  }

  return db.cards;
}

function saveCards(cards){
  var db = localStorage.getItem("db");

  if(db != undefined && db != null){
    db = JSON.parse(db);
    db.cards = cards;
  }else{
    db = {
      "cards" : cards
    };
  }

  localStorage.setItem("db", JSON.stringify(db));
}

function createCard(title, content){
  var db = localStorage.getItem("db");

  if(title != undefined && content != undefined){

    if(db != undefined && db != null){
      db = JSON.parse(db);

      db.cards[db.cards.length] = {"title" : title, "content" : content};
      localStorage.setItem("db", JSON.stringify(db));

    }else{
      db = {
        "cards" : [{"title" : title, "content" : content}]
      };

      localStorage.setItem("db", JSON.stringify(db));

    }

  }

}

function updateCard(index, titulo, conteudo){
  var cards = getCards();
  if(cards[index] != undefined && cards[index] != null && (titulo != "" || conteudo != "") ){

    cards[index].title = titulo;
    cards[index].content = conteudo;
    saveCards(cards);
  }

}

function deleteCards(index){
  var cards = getCards();
  if(cards[index] != undefined && cards[index] != null){
    cards.splice(index, 1);
    saveCards(cards);
    loadCards();
  }
}
